import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container, Row } from 'react-bootstrap'
import './css/styletdc.css'
import ProductList from './components/ProductList/productlist'
import callApi from './utils/apiCaller'
import routes from './routers'
import {
    Switch,
    Link,
    Route,
    BrowserRouter as Router
} from 'react-router-dom';


class App extends React.Component {
    render() {
        return ( <Router > { this.showContentmenus(routes) } </Router>
        );
    }
    showContentmenus = (routes) => {
        var result = null;
        if (routes.length > 0) {
            result = routes.map((route, index) => {
                return ( <Route key = { index }
                    path = { route.path }
                    exact = { route.exact }
                    component = { route.main }/>)
            });
        }
        return <Switch > { result } </Switch>
    }
}

export default App;