import Axios from 'axios';
import axios from 'axios';
import * as Config from './../constants/Config';
import * as Types from './../constants/TypesAction';

//---------------------------------------------------------------------
export const AppNewUpdateFetchRequest = () => {
    return dispatch => {
        return axios.get(`${Config.API_URL}/tdcmobileapp/newupdate`,null,null).then(res => {
            if(res.status === 200) {
                dispatch(AppNewUpdateFetch(res.data))
            }
        })
    }
}
export const AppNewUpdateFetch = (appnewupdate) => {
    return {
        type: Types.APP_NEWUPDATE,
        appnewupdate
    }
}

//---------------------------------------------------------------------
export const AppNewCreateFetchRequest = () => {
    return dispatch => {
        return axios.get(`${Config.API_URL}/tdcmobileapp/newcreate`,null,null).then(res => {
            if(res.status === 200) {
                dispatch(AppNewCreateFetch(res.data))
            }
        })
    }
}
export const AppNewCreateFetch = (appnewcreate) => {
    return {
        type: Types.APP_NEWCREATE,
        appnewcreate
    }
}

//---------------------------------------------------------------------
export const AppDetailsFetchRequest = (id_app) => {
    return dispatch => {
        return axios.get(`${Config.API_URL}/tdcmobileapp/${id_app}`,null,null).then(res => {
            if(res.status === 200) {
                dispatch(AppDetailsFetch(res.data))
            }
        })
    }
}
export const AppDetailsFetch = (appdetail) => {
    return {
        type: Types.APP_DETAIL,
        appdetail
    }
}

//---------------------------------------------------------------------
export const AppVersionsFetchRequest = (id_app) => {
    return dispatch => {
        return axios.get(`${Config.API_URL}/tdcmobileapp/versionlist/${id_app}`,null,null).then(res => {
            if(res.status === 200) {
                dispatch(AppVersionsFetch(res.data))
            }
        })
    }
}
export const AppVersionsFetch = (appversions) => {
    return {
        type: Types.APP_VERSIONS,
        appversions
    }
}

