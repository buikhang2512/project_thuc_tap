import axios from 'axios';
import { Alert } from 'react-bootstrap';
import * as Config from './../constants/Config';

export default function userCallerApi(endpoint, method = 'GET', body, header) {
    let url = `${Config.API_URL}/${endpoint}`;
    const token = localStorage.getItem('jwt_token');
    const resetpass = localStorage.getItem('resetpass_token');
    console.log(url);
    console.log(token);
    return axios({
        method: method,
        url: url,
        data: body,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'jwt_token': token,
            'resetpass_token':resetpass,
            header,
        }
    }).catch(err => {
        if (!err.status) {
            alert('Bạn phải đăng nhập!', err);
        }
    });
}