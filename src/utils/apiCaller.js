import axios from 'axios';
import * as Config from './../constants/Config';

export default function callApi(endpoint, method = 'GET', body) {
    let url = `${Config.API_URL}/${endpoint}`;
    //console.log(url);
    return axios({
        method: method,
        url: url,
        data: body,
    }).catch(err => {
        if (!err.status) {
            alert('Lỗi! Hông thể kết nối với máy chủ');
        }
    });
}