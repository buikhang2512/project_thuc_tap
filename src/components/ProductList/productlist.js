import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card,Button,Row } from 'react-bootstrap';
import { Switch, Link, Route, BrowserRouter as Router } from 'react-router-dom';
import { API_URL } from './../../constants/Config';
import fecha from 'fecha';

class ProductList extends Component{

    render() {
        var {product} = this.props;
        var img_product = `${API_URL}/${product.iconURL}`;
        const day = product.created_at.substr(0, 10);
        const stringtoday = fecha.parse(day, 'YYYY-MM-DD')
        const formatday = fecha.format(stringtoday, 'DD-MM-YYYY')

        return (
            <div className="col-lg-4 col-md-4 col-12" style={{float:"left",paddingTop:"10px"}}>
                {/* <Link to={product._id} className="btn btn-light"  onClick={ () => {this.onClick(this.props.name)}}>*/} 
                    {/* <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <img src={img_product} className="img-fluid" />
                    <p className="fis">{product.name}</p>
                </Link> */}
                <Link to={product._id} className='product-card'>
                    <Card className='card-border-bot'>
                        <div className='card-img'>
                            <img src={img_product}style={{width:'100%'}}/>
                        </div>
                        <div >
                        <div className='card-group'>
                            <p className='card-title'>{product.name}</p>
                            <p className='card-bundlename'>{product.bundleName}</p>
                            <p className='card-bundlename-1'>{formatday}</p>
                        </div>
                        </div>
                    </Card>
                </Link>
            </div>
        );
    }
}

export default ProductList;