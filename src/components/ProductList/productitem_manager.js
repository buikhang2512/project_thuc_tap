import React from 'react';
import { Container, Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { API_URL } from './../../constants/Config';
import {Link} from 'react-router-dom';

class ProductItemMagager extends React.Component {

    // khi click thì truyen vao ondelete 1 cai id cua san pham do
    // hàm confirm dùng để xuất ra thông báo nếu người dùng click ok thì id mới truyền được vào props onDelelte
    // muon sai hàm confirm thì phai co dòng -eslint-disable-line thì code mới chạy nếu không sẽ bị lỗi
    onDelete = (id) => {
        if(confirm('Bạn chắc chắn muốn xoá ?')) { // eslint-disable-line
            this.props.onDelete(id); //truyền id vào props onDelete -- và qua bên productList_manager để nhận lại
        }       
        // callApi(`Products/${id}`,'DELETE',null);
    }
    //Update cũng gần tương tự như delete thôi
    onUpdate = (id) => {
        this.props.onUpdate(id);
    }

    render() {
        var {productmng,index } = this.props;
        var img_product = `${API_URL}/${productmng.iconURL}`;
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{productmng.name}</td>
                <td>{productmng.bundleName}</td>
                <td><img src={img_product} className="rounded"/></td>
                {/* <td>
                    <span className='label label-success'>
                        tao lao
                    </span>
                </td> */}
                <td>

                    <Link to={`/product/${productmng._id}/edit`} type="button" className="btn btn-info" style={{ marginRight: '10px' }} >Edit</Link>

                    <Link to={`/product/versions/${productmng._id}`} type="button" className="btn btn-warning" style={{ marginRight: '10px' }} >Upload</Link>

                    <button type="button" className="btn btn-danger" onClick={ () => this.onDelete(productmng._id)}>Delete</button>

                </td>
            </tr> 
            
        );
    }

}

export default ProductItemMagager;