import React from 'react';
import { Container, Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { API_URL } from './../../constants/Config';
import {Link} from 'react-router-dom';
import fecha from 'fecha';

class VersionItemMagager extends React.Component {

    // khi click thì truyen vao ondelete 1 cai id cua san pham do
    // hàm confirm dùng để xuất ra thông báo nếu người dùng click ok thì id mới truyền được vào props onDelelte
    // muon sai hàm confirm thì phai co dòng -eslint-disable-line thì code mới chạy nếu không sẽ bị lỗi
    onDelete = (id) => {
        if(confirm('Bạn chắc chắn muốn xoá ?')) { // eslint-disable-line
            this.props.onDelete(id); //truyền id vào props onDelete -- và qua bên productList_manager để nhận lại
        }       
        // callApi(`Products/${id}`,'DELETE',null);
    }
    //Update cũng gần tương tự như delete thôi
    onUpdate = (id,name,des) => {
        this.props.onUpdate(id,name,des);
    }

    render() {
        var {versionmng,index } = this.props;
        let ios = `${API_URL}/${versionmng.ios}`;
        let android = `${API_URL}/${versionmng.android}`;
        const day = versionmng.created_at.substr(0, 10);
        const stringtoday = fecha.parse(day, 'YYYY-MM-DD')
        const formatday = fecha.format(stringtoday, 'DD/MM/YYYY')
        console.log(versionmng.descripstion);
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{versionmng.version}</td>
                <td>{formatday}</td>
                <td>
                    {/* <Link to='#' type="button" className="btn btn-info" style={{ marginRight: '10px' }} >Sửa
                    <span>  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-tools" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M0 1l1-1 3.081 2.2a1 1 0 0 1 .419.815v.07a1 1 0 0 0 .293.708L10.5 9.5l.914-.305a1 1 0 0 1 1.023.242l3.356 3.356a1 1 0 0 1 0 1.414l-1.586 1.586a1 1 0 0 1-1.414 0l-3.356-3.356a1 1 0 0 1-.242-1.023L9.5 10.5 3.793 4.793a1 1 0 0 0-.707-.293h-.071a1 1 0 0 1-.814-.419L0 1zm11.354 9.646a.5.5 0 0 0-.708.708l3 3a.5.5 0 0 0 .708-.708l-3-3z"/>
                        <path fill-rule="evenodd" d="M15.898 2.223a3.003 3.003 0 0 1-3.679 3.674L5.878 12.15a3 3 0 1 1-2.027-2.027l6.252-6.341A3 3 0 0 1 13.778.1l-2.142 2.142L12 4l1.757.364 2.141-2.141zm-13.37 9.019L3.001 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026z"/>
                    </svg></span> 
                    </Link> */}
                    <button type="button" className="btn btn-info" style={{ marginRight: '10px' }} onClick={ () => this.onUpdate(versionmng._id,versionmng.version,versionmng.description)}>Edit</button>
                    <button type="button" className="btn btn-danger" onClick={ () => this.onDelete(versionmng._id)}>Delete</button>
                </td>
            </tr> 
            
        );
    }

}

export default VersionItemMagager;