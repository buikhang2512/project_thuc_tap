import React, { Component } from 'react';
import { Switch, Link, Route, BrowserRouter as Router } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { API_URL } from './../../constants/Config';
import callApi from './../../utils/apiCaller';
import moment from 'moment';
import fecha from 'fecha';
class Version extends Component {
    render() {
        var { version } = this.props;
        let ios = `${API_URL}/${version.ios}`;
        let android = `${API_URL}/${version.android}`;
        const day = version.created_at.substr(0, 10);
        const stringtoday = fecha.parse(day, 'YYYY-MM-DD');
        const formatday = fecha.format(stringtoday, 'DD/MM/YYYY');
        return ( 
            <tr>
                <td style = {{ color: '#0334FF' } } > { version.version } </td> 
                <td > { formatday } </td> 
                <td >
                    <div className = "r" >
                        <Button variant = "warning"className = "vdownload"href = { ios } > IOS </Button> 
                        <Button variant = "success"className = "vdownload"href = { android } > Android </Button> 
                    </div> 
                </td> 
            </tr>
        )

    }
}
export default Version;