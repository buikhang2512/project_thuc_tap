import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
class menuChild extends Component {
    render() {
        return (
            <div className="menu">
            <Row>
            <ul className="nav nav-tabs " style={{ border: 'none', display: 'inline-flex' }} >
                <li className="nav-item ">
                    <Link className="nav-link active" href="#q" data-toggle="tab">Trang chủ <span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" href="#s" data-toggle="tab">Sản phẩm</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" href="#d" data-toggle="tab">Giới thiệu</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" href="#s" data-toggle="tab">Liên hệ</Link>
                </li>
            </ul>
            </Row>
           
        </div>
        );
    }
}
export default menuChild;
