import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
class SearchApp extends Component {
    offSearch=()=>{
        this.props.offSearch();
   

    }
    render() {
        return (
            <Row>
                <div className="col-9"><input className="input-search" placeholder="Tìm kiếm"/></div>
                <div className="col-1">
                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" className="bi bi-search1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
                        <path fillRule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
                    </svg></div>
                <div className="col-1">
                    <svg width="4em" height="2em" onClick={this.offSearch} viewBox="0 0 16 16" className="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg></div>
                   
            </Row>
        );
    }
}
export default SearchApp;
