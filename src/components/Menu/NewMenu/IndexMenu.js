import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import logo from './../../../img/logo.png';
import user from './../../../img/user.png';
import './../../../css/menuNew.css';
import MenuDesk from './menuDesk';
import SearchDesk from './searchDesk';
import SearchApp from './SearchApp';
import MenuApp from './menuApp';


class menuNew extends Component {
  
    showSearch = () => {
        this.props.showSearch();
      
    }
    offSearch = () => {
       this.props.offSearch();
       
    }

    onSearchApp = () => {
       this.props.onSearchApp();

    }
  
    render() {
       
        
        var {idDisplayform } = this.props;
       
        var element = idDisplayform ? <MenuDesk style={{ display: 'initial' }} /> : <SearchDesk style={{ display: 'initial' }}  tatSearch={this.offSearch}  />
        var iconSearch = idDisplayform ? <svg width="2em" height="2em" onClick={this.showSearch} viewBox="0 0 16 16" className="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
            <path fillRule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
        </svg> : "";
        var searchApp = idDisplayform ? <MenuApp onSearch={this.onSearchApp}/> :<SearchApp offSearch={this.offSearch} /> ;
        return (
            <div className="header">
                <div className="container-fluid">
                    <div className="areaDesktop">
                        <nav className="navbar navbar-expand-lg navbar-light bg-light">
                            <div className="col-lg-1 col-md-1 col-1">
                                <img src={logo} alt="logo" className="logo" />
                            </div>
                            <div className="col-lg-8 col-md-8 col-8">
                                {element}
                            </div>
                            <div className="col-lg-3 col-md-3 col-3">
                                <div className="header-right">
                                    <div className="row">
                                        {iconSearch}
                                        <img src={user} alt='user' className='userimg' />
                                        <Link className="btn-login" to="/loginadmin">Đăng nhập</Link>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div className="areaApp">
                        {searchApp}
                    </div>

                </div>
            </div>


        );
    }
}
export default menuNew;