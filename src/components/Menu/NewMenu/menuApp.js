import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
class headApp extends Component {
    onSearch = () => {
        this.props.onSearch();
    }
    render() {
        return (
            <div>
                <Row>
                    <div className="col-2">
                        <span data-toggle="collapse" data-target="#menuleft">
                            <svg width="2.5em" height="2.5em" viewBox="0 0 16 16" className="bi bi-filter" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
                            </svg>

                        </span>
                    </div>
                    <div className="col-8">
                        <b>  TDC Mobile AppStore</b>
                    </div>
                    <div className="col-2">
                        <svg width="2em" onClick={this.onSearch} height="2em" viewBox="0 0 16 16" className="bi bi-search1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
                            <path fillRule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
                        </svg>
                    </div>

                </Row>
                <div className="collapse" id="menuleft">
                    <li><Link> Trang chủ</Link> <span> <svg width={6} height={12} style={{ float: 'right' }} viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5288 5.64517L0.477851 0.690536C0.464653 0.677486 0.448791 0.669376 0.432088 0.667137C0.415385 0.664899 0.398518 0.668622 0.383424 0.67788C0.368331 0.687138 0.355622 0.701554 0.346758 0.719474C0.337894 0.737394 0.333234 0.758091 0.333314 0.779187V1.86693C0.333314 1.93588 0.359084 2.00202 0.401661 2.04423L4.43525 5.99978L0.401661 9.95533C0.357964 9.99754 0.333314 10.0637 0.333314 10.1326V11.2204C0.333314 11.3146 0.419588 11.3667 0.477851 11.309L5.5288 6.35438C5.57173 6.31233 5.60646 6.25853 5.63034 6.19708C5.65423 6.13564 5.66665 6.06816 5.66665 5.99978C5.66665 5.9314 5.65423 5.86392 5.63034 5.80247C5.60646 5.74103 5.57173 5.68723 5.5288 5.64517Z" fill="#8C8C8C" />
                    </svg></span></li>
                    <li><Link> Sản phẩm</Link> <span> <svg width={6} height={12} style={{ float: 'right' }} viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5288 5.64517L0.477851 0.690536C0.464653 0.677486 0.448791 0.669376 0.432088 0.667137C0.415385 0.664899 0.398518 0.668622 0.383424 0.67788C0.368331 0.687138 0.355622 0.701554 0.346758 0.719474C0.337894 0.737394 0.333234 0.758091 0.333314 0.779187V1.86693C0.333314 1.93588 0.359084 2.00202 0.401661 2.04423L4.43525 5.99978L0.401661 9.95533C0.357964 9.99754 0.333314 10.0637 0.333314 10.1326V11.2204C0.333314 11.3146 0.419588 11.3667 0.477851 11.309L5.5288 6.35438C5.57173 6.31233 5.60646 6.25853 5.63034 6.19708C5.65423 6.13564 5.66665 6.06816 5.66665 5.99978C5.66665 5.9314 5.65423 5.86392 5.63034 5.80247C5.60646 5.74103 5.57173 5.68723 5.5288 5.64517Z" fill="#8C8C8C" />
                    </svg></span></li>
                    <li><Link> Giới thiệu</Link> <span> <svg width={6} height={12} style={{ float: 'right' }} viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5288 5.64517L0.477851 0.690536C0.464653 0.677486 0.448791 0.669376 0.432088 0.667137C0.415385 0.664899 0.398518 0.668622 0.383424 0.67788C0.368331 0.687138 0.355622 0.701554 0.346758 0.719474C0.337894 0.737394 0.333234 0.758091 0.333314 0.779187V1.86693C0.333314 1.93588 0.359084 2.00202 0.401661 2.04423L4.43525 5.99978L0.401661 9.95533C0.357964 9.99754 0.333314 10.0637 0.333314 10.1326V11.2204C0.333314 11.3146 0.419588 11.3667 0.477851 11.309L5.5288 6.35438C5.57173 6.31233 5.60646 6.25853 5.63034 6.19708C5.65423 6.13564 5.66665 6.06816 5.66665 5.99978C5.66665 5.9314 5.65423 5.86392 5.63034 5.80247C5.60646 5.74103 5.57173 5.68723 5.5288 5.64517Z" fill="#8C8C8C" />
                    </svg></span></li>
                    <li><Link> Liên hệ</Link> <span> <svg width={6} height={12} style={{ float: 'right' }} viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5288 5.64517L0.477851 0.690536C0.464653 0.677486 0.448791 0.669376 0.432088 0.667137C0.415385 0.664899 0.398518 0.668622 0.383424 0.67788C0.368331 0.687138 0.355622 0.701554 0.346758 0.719474C0.337894 0.737394 0.333234 0.758091 0.333314 0.779187V1.86693C0.333314 1.93588 0.359084 2.00202 0.401661 2.04423L4.43525 5.99978L0.401661 9.95533C0.357964 9.99754 0.333314 10.0637 0.333314 10.1326V11.2204C0.333314 11.3146 0.419588 11.3667 0.477851 11.309L5.5288 6.35438C5.57173 6.31233 5.60646 6.25853 5.63034 6.19708C5.65423 6.13564 5.66665 6.06816 5.66665 5.99978C5.66665 5.9314 5.65423 5.86392 5.63034 5.80247C5.60646 5.74103 5.57173 5.68723 5.5288 5.64517Z" fill="#8C8C8C" />
                    </svg></span></li>
                    <li className="li-dangNhap"><Link to="/loginadmin">Đăng nhập</Link> <span> <svg width={6} height={12} style={{ float: 'right' }} viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5288 5.64517L0.477851 0.690536C0.464653 0.677486 0.448791 0.669376 0.432088 0.667137C0.415385 0.664899 0.398518 0.668622 0.383424 0.67788C0.368331 0.687138 0.355622 0.701554 0.346758 0.719474C0.337894 0.737394 0.333234 0.758091 0.333314 0.779187V1.86693C0.333314 1.93588 0.359084 2.00202 0.401661 2.04423L4.43525 5.99978L0.401661 9.95533C0.357964 9.99754 0.333314 10.0637 0.333314 10.1326V11.2204C0.333314 11.3146 0.419588 11.3667 0.477851 11.309L5.5288 6.35438C5.57173 6.31233 5.60646 6.25853 5.63034 6.19708C5.65423 6.13564 5.66665 6.06816 5.66665 5.99978C5.66665 5.9314 5.65423 5.86392 5.63034 5.80247C5.60646 5.74103 5.57173 5.68723 5.5288 5.64517Z" fill="#8C8C8C" />
                    </svg></span></li>
                </div>
            </div>
        );
    }
}
export default headApp;