import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import logo from './../../../img/logo.png';
import user from './../../../img/user.png';
import './../../../css/menuNew.css';
import MenuChild from '../NewMenu/menuDesk';
import Search from '../NewMenu/searchDesk';
import SearchApp from '../NewMenu/SearchApp';
import MenuUserApp from '../NewMenuUser/menuUserApp';


class menuNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDisplayForm: true
        }
    }
    showMenu = () => {
        this.setState({
            isDisplayForm: !this.state.isDisplayForm
        })
    }
    offSearch = () => {
        this.setState({
            isDisplayForm: !this.state.isDisplayForm
        });

    }
    offSearch = () => {
        this.setState({
            isDisplayForm: !this.state.isDisplayForm
        });
    }
    onSearch = () => {
        this.setState({
            isDisplayForm: !this.state.isDisplayForm
        });

    }
    render() {
        var { isDisplayForm } = this.state;
        var element = isDisplayForm ? <MenuChild style={{ display: 'initial' }} /> : <Search style={{ display: 'initial' }} tatSearch={this.offSearch} />
        var iconSearch = isDisplayForm ? <svg width="2em" height="2em" onClick={this.showMenu} viewBox="0 0 16 16" className="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
            <path fillRule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
        </svg> : "";
        var searchApp = isDisplayForm ? <MenuUserApp onSearch={this.onSearch} /> : <SearchApp offSearch={this.offSearch} />;
        return (
            <div className="header">
                <div className="container-fluid">
                    <div className="areaDesktop">
                        <nav className="navbar navbar-expand-lg navbar-light bg-light">
                            <div className="col-lg-1 col-md-1 col-1">
                                <img src={logo} alt="logo" className="logo" />
                            </div>
                            <div className="col-lg-8 col-md-8 col-8">
                                {element}
                            </div>
                            <div className="col-lg-3 col-md-3 col-3">
                                <div className="header-right">
                                    <div className="row">
                                        {iconSearch}
                                        <span className="chuong"><svg width="18" height="20" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.5455 8.29979V6.63983C15.5455 4.87884 14.8558 3.18997 13.6283 1.94476C12.4008 0.699552 10.736 0 9 0C7.26404 0 5.59918 0.699552 4.37166 1.94476C3.14415 3.18997 2.45455 4.87884 2.45455 6.63983V8.29979C2.43445 8.80605 2.31168 9.30267 2.09391 9.75865C1.87613 10.2146 1.56802 10.6201 1.18882 10.9499C0.507433 11.5389 0.0806595 12.3752 0 13.2797C0 15.3546 3.36436 16.5996 9 16.5996C14.6356 16.5996 18 15.3546 18 13.2797C17.9193 12.3752 17.4926 11.5389 16.8112 10.9499C16.432 10.6201 16.1239 10.2146 15.9061 9.75865C15.6883 9.30267 15.5655 8.80605 15.5455 8.29979Z" fill="#E8E8E8" />
                                            <path d="M6.66411 18.2595C6.82005 18.7642 7.13071 19.2051 7.55086 19.5181C7.97101 19.8312 8.47871 20 9.00002 20C9.52132 20 10.029 19.8312 10.4492 19.5181C10.8693 19.2051 11.18 18.7642 11.3359 18.2595C10.6045 18.3143 9.8272 18.3425 9.00002 18.3425C8.17283 18.3425 7.39556 18.316 6.66411 18.2595Z" fill="#E8E8E8" />
                                        </svg>
                                        </span>
                                        <img src={user} alt='user' className='userimg' />
                                        <Link className="btn-login" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Vo Duc
                                      <span> <svg width="12" height="6" viewBox="0 0 12 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M11.2214 0.333313H10.1688C10.0972 0.333313 10.0298 0.359355 9.9877 0.402063L6.00026 4.48123L2.01283 0.402063C1.97072 0.359355 1.90335 0.333313 1.83177 0.333313H0.779124C0.687895 0.333313 0.634561 0.410396 0.687895 0.465605L5.63675 5.52915C5.8164 5.71248 6.18413 5.71248 6.36237 5.52915L11.3112 0.465605C11.366 0.410396 11.3126 0.333313 11.2214 0.333313Z" fill="#1890FF" />
                                            </svg>
                                            </span>
                                        </Link>

                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        <div className="row" style={{float:'right'}}>
                            <ul className="collapse multi-collapse" id="multiCollapseExample1" >
                                <li><Link>Thông tin cá nhân</Link></li>
                                <li><Link>Chức năng số 2</Link></li>
                                <li><Link>Chức năng số 3</Link></li>
                                <li><Link className="p-dangxuat">Đăng xuất</Link><span><svg style={{ float: 'right', marginTop:'0.5rem' ,marginLeft:'1rem'}} width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.8797 9.83438H11.0391C10.9734 9.83438 10.9187 9.88906 10.9187 9.95469V10.9203H1.07969V1.07969H10.9203V2.04531C10.9203 2.11094 10.975 2.16563 11.0406 2.16563H11.8813C11.9469 2.16563 12.0016 2.1125 12.0016 2.04531V0.479687C12.0016 0.214062 11.7875 0 11.5219 0H0.479687C0.214062 0 0 0.214062 0 0.479687V11.5203C0 11.7859 0.214062 12 0.479687 12H11.5203C11.7859 12 12 11.7859 12 11.5203V9.95469C12 9.8875 11.9453 9.83438 11.8797 9.83438ZM12.1703 5.90156L9.95312 4.15156C9.87031 4.08594 9.75 4.14531 9.75 4.25V5.4375H4.84375C4.775 5.4375 4.71875 5.49375 4.71875 5.5625V6.4375C4.71875 6.50625 4.775 6.5625 4.84375 6.5625H9.75V7.75C9.75 7.85469 9.87187 7.91406 9.95312 7.84844L12.1703 6.09844C12.1853 6.08674 12.1973 6.0718 12.2056 6.05475C12.214 6.03769 12.2183 6.01897 12.2183 6C12.2183 5.98103 12.214 5.96231 12.2056 5.94525C12.1973 5.92819 12.1853 5.91326 12.1703 5.90156Z" fill="#FF4D4F" />
                                </svg></span></li>
                            </ul>
                        </div>
                    </div>
                    <div className="areaApp">
                        {searchApp}
                    </div>

                </div>
            </div>


        );
    }
}
export default menuNew;