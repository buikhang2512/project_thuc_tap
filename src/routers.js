import React from 'react';
import { Redirect } from 'react-router-dom';
import AdminTDC from './pages/Admin/AdminTDC';
import ChangePassword from './pages/ChangPassword/ChangPassword';
import ChangePass from './pages/FogotPass/ChangePass';
import FogotPass from './pages/FogotPass/FogotPass';
import HomePage from './pages/HomePage/homepage';
import InformationUser from './pages/informationUser/informationUser';
import LoginPage from './pages/LoginAminPage/login';
import NotFoundPage from './pages/NotFoundPage/notFoundPage';
import ProductActionPage from './pages/ProductActionPage/productActionPage';
import ProductListManagerPage from './pages/ProductListManagerPage/productlist_managerpage';
import VersionListPage from './pages/ProductListPage/VersionListPage';
import VersionListManagerPage from './pages/VersionListManagerPage/versionlistmanagerpage';
import TrangChu from './pages/HomePage/TrangChu'
const routes = [{
    path: '/',
    exact: true,
    main: () => < HomePage />
},
{
    path: '/admintdc',
    exact: true,
    main: () => < AdminTDC />
},
{
    path: '/loginadmin',
    exact: false,
    main: ({ history }) => < LoginPage history={history} />
},
{
    path: '/TrangChu',
    exact: false,
    main: ()=><TrangChu />
},
{
    path: '/product/versions/:id',
    exact: false,
    main: ({ match,history }) => < VersionListManagerPage match={match} history={history} />
},
{
    path: '/productmanager',
    exact: false,
    main: ({ history }) => <ProductListManagerPage history={history} />
},
{
    path: '/changePass',
    exact: false,
    main: ({ history }) => < ChangePass history={history} />
},
{
    path: '/forgotPass',
    exact: false,
    main: ({ history }) => < FogotPass history={history} />
},
{
    path: '/doipass',
    exact: false,
    main: ({ history }) => <ChangePassword history={history} />
},
{
    path: '/user',
    exact: false,
    main: ({ history }) => <InformationUser history={history} />
},
{
    path: '/product/add',
    exact: false,
    main: ({history}) => < ProductActionPage history={history}/>
},
{
    path: '/product/:id/edit',
    exact: false,
    main: ({ match,history }) => < ProductActionPage match={match} history={history}/>
},
{
    path: '/:id',
    exact: false,
    main: ({ match }) => < VersionListPage match={match}
    />
},
{
    path: '',
    exact: false,
    main: () => < NotFoundPage />
},
]

export default routes;