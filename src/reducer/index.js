import AppNewCreate from './../reducer/AppNewCreate';
import AppNewUpdate from './../reducer/AppNewUpdate';
import AppDetail from './../reducer/AppDetail';
import AppVersions from './../reducer/AppVersions';

const { combineReducers } = require("redux");

var reducer = combineReducers({
    AppNewCreate,
    AppNewUpdate,
    AppDetail,
    AppVersions,
})
export default reducer;