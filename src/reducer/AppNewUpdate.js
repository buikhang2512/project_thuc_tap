import * as Types from './../constants/TypesAction';

const initialState = '';

var myReducer = (state = initialState ,action) => {
    switch(action.type){
        case Types.APP_NEWUPDATE:
            state = action.appnewupdate;
            return [...state];
        default:
            return state;
    }
}
export default myReducer;