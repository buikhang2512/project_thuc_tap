import * as Types from './../constants/TypesAction';

const initialState = '';

var myReducer = (state = initialState ,action) => {
    switch(action.type){
        case Types.APP_VERSIONS:
            state = action.appversions;
            return [...state];
        default:
            return state;
    }
}
export default myReducer;