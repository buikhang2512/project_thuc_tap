import React from 'react';
import './../../css/styletdc.css';
import { Button, FormControl, Nav, Form, Navbar } from 'react-bootstrap';
class NotFoundPage extends React.Component {
    render() {
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div className="btn btn-neon">
                <strong><h1>404! Không tìm thấy trang !!!</h1></strong> 
            </div>
            <a href="/" className="btn btn-neon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Quay về trang chủ
            </a>
            </div>
        );
    }

}

export default NotFoundPage;