import React, { Component } from 'react';
import userCallerApi from '../../utils/userCallerApi';
import './../../css/ChangePassword.css';
class ChangePassword extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            passwordnew: '',
            passconfirm: '',
        }
    }
    onSubmit = (event) => {
        event.preventDefault();
        var {history} = this.props;
        var { passconfirm, passwordnew, password } = this.state;
        var email = localStorage.getItem('email');
        if (passwordnew === passconfirm && (passwordnew !== password)) {

            userCallerApi(`change/${email}`, 'POST', { password, passwordnew }).then((res) => {
                if (res) {
                    if (res.status === 200) {
                        alert('Thay đổi mật khẩu thành công!');
                        history.push('/user');
                    }                 
                }
                else {
                    alert('Mật khẩu cũ không đúng!');
                }

            });
        }
        else {
            alert('Mật khẩu không trùng khớp!');
        }
    }
    onChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    myFunction=()=> {
        var pw_ele = document.getElementById("myInput");
        if (pw_ele.type === "password") {
          pw_ele.type = "text";
        } else {
          pw_ele.type = "password";
        }
      }
    render() {
        var { password, passwordnew, passconfirm } = this.state;

        return (
            <div className="doipass">
                <div className="col col-6 doi">
                    <div className="content-right aboutpass">
                        <h3 className="thongtin">Đổi mật khẩu</h3>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group row">

                                <div className="wrap-input">
                                    <span className="lable-input2" >Mật khẩu cũ </span>
                                    <span className="haicham" >:</span>
                                    <input className="input100" type="password"  id="myInput" value={password} onChange={this.onChange} name="password"></input>
                                    <span style={{marginTop: "25px"}} onClick={this.myFunction}>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-eye-slash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z" />
                                        <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z" />
                                        <path fillRule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z" />
                                    </svg>
                                    </span>
                                    
                                </div>

                                <div className="wrap-input">
                                    <span className="lable-input2">Mật khẩu mới  </span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" value={passwordnew} onChange={this.onChange} name="passwordnew"></input>
                                </div>
                                <div className="wrap-input">
                                    <span className="lable-input2">xác nhận mật khẩu mới </span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" value={passconfirm} onChange={this.onChange} name="passconfirm"></input>
                                </div>

                                <button type="reset" className="btn btn-info mr-10">Làm lại</button>
                                <button type="submit" className="btn btn-success mr-10">Đổi mật khẩu</button>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        );
    }
}
export default ChangePassword;
