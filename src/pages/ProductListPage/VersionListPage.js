import React, { version } from 'react';
import { Row } from 'react-bootstrap';
import { API_URL } from './../../constants/Config';
import callApi from './../../utils/apiCaller';
import ReactHtmlParser from 'react-html-parser';
import ReactPaginate from 'react-paginate';


class VersionListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            iconURL: '',
            bundleName: '',
            description: '',
            versions: [],
            android: '',
            ios: '',
            images: [],
            offset: 0,
            perPage: 6,
            currentPage: 0
        };
        this.handlePageClick = this
            .handlePageClick
            .bind(this);
    }

    receivedData() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            callApi(`tdcmobileapp/versionlist/${id}`)
                .then(res => {
                    const data = res.data.record;
                    console.log('oke');      
                    const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
                    const postData = slice.map(pd =>
                        <React.Fragment>
                            <div className="col-xl-6 col-lg-6 col-sm-12" style={{ width: "100%" }}>
                                <div className="phienbankhac">
                                    <p style={{ color: "#2F6BFF", }}>{pd.version}</p>
                                    <p style={{ marginTop: "-10px" }}>{ReactHtmlParser(pd.descripstion)}
                                    </p>
                                    <a href={pd.ios}>
                                        <button className="btn btn-version btn-ios" style={{ marginRight: '10px', lineHeight: "16px" }}>
                                            <span>
                                                <svg width="11" height="25" viewBox="0 1 10 12" fill="none" xmlns="http://www.w3.org/2000/svg" style={{ paddingBottom: "10px" }}>
                                                    <path d="M8.5194 6.79614C8.51363 5.81252 8.95928 5.07121 9.85925 4.52459C9.3559 3.80347 8.5944 3.40685 7.59059 3.33041C6.64014 3.25541 5.60028 3.88423 5.21953 3.88423C4.81714 3.88423 3.89698 3.35637 3.17297 3.35637C1.6788 3.37945 0.0908813 4.54767 0.0908813 6.9245C0.0908813 7.62688 0.219242 8.35233 0.475962 9.09942C0.819218 10.083 2.05667 12.493 3.34748 12.4541C4.02246 12.4382 4.49984 11.9753 5.37817 11.9753C6.23055 11.9753 6.67187 12.4541 7.42473 12.4541C8.72708 12.4353 9.84627 10.2446 10.1722 9.25806C8.42565 8.43454 8.5194 6.84662 8.5194 6.79614ZM7.00359 2.39727C7.73481 1.52904 7.66847 0.738683 7.64684 0.454559C7.00071 0.492058 6.25362 0.894446 5.82816 1.38914C5.35943 1.91989 5.08396 2.57611 5.14309 3.31599C5.84114 3.36935 6.47861 3.01023 7.00359 2.39727Z" fill="white" />
                                                </svg>
                                            </span>
                                        </button>
                                    </a>
                                    <a href={pd.android}>
                                        <button className="btn btn-version btn-android" style={{ paddingBottom: "10px" }}>
                                            <span>
                                                <svg width="11" height="25" viewBox="0 2 10 12" fill="none" xmlns="http://www.w3.org/2000/svg" style={{ paddingBottom: "7px" }}>
                                                    <path d="M1.90622 8.66429C1.90622 8.96343 2.1504 9.20761 2.45082 9.20761H3.07341V10.7468C3.07341 11.1367 3.38662 11.4551 3.77142 11.4551C4.1575 11.4551 4.46943 11.138 4.46943 10.7468V9.20633H5.55608V10.7455C5.55608 11.1355 5.86929 11.4538 6.25409 11.4538C6.64017 11.4538 6.9521 11.1367 6.9521 10.7455V9.20633H7.57469C7.87511 9.20633 8.11929 8.96216 8.11929 8.66301V3.61074H1.90622V8.66429ZM6.47142 0.992555L7.04543 0.161589C7.07867 0.11301 7.071 0.0478109 7.02625 0.0158507C6.98151 -0.0148311 6.91759 0.000509842 6.88435 0.0490894L6.28861 0.913294C5.89614 0.758606 5.45892 0.672953 4.99997 0.672953C4.54102 0.672953 4.10381 0.758606 3.71134 0.913294L3.1156 0.0503678C3.08236 0.00178823 3.01844 -0.0148311 2.97369 0.0171292C2.92895 0.047811 2.92128 0.111731 2.95452 0.162868L3.52852 0.993833C2.61574 1.41827 1.97653 2.22238 1.89472 3.16201H8.10779C8.02341 2.22111 7.38293 1.41699 6.47142 0.992555ZM3.68832 2.30292C3.62031 2.30292 3.55382 2.28276 3.49727 2.24497C3.44072 2.20718 3.39664 2.15347 3.37061 2.09063C3.34458 2.0278 3.33777 1.95865 3.35104 1.89194C3.36431 1.82523 3.39706 1.76396 3.44516 1.71586C3.49325 1.66777 3.55453 1.63502 3.62123 1.62175C3.68794 1.60848 3.75709 1.61529 3.81993 1.64132C3.88276 1.66735 3.93647 1.71142 3.97426 1.76798C4.01205 1.82453 4.03222 1.89102 4.03222 1.95903C4.03198 2.05017 3.99567 2.1375 3.93123 2.20194C3.86679 2.26638 3.77946 2.30269 3.68832 2.30292ZM6.35253 2.30292C6.28451 2.30292 6.21803 2.28276 6.16147 2.24497C6.10492 2.20718 6.06084 2.15347 6.03481 2.09063C6.00879 2.0278 6.00198 1.95865 6.01524 1.89194C6.02851 1.82523 6.06127 1.76396 6.10936 1.71586C6.15746 1.66777 6.21873 1.63502 6.28544 1.62175C6.35215 1.60848 6.42129 1.61529 6.48413 1.64132C6.54697 1.66735 6.60068 1.71142 6.63846 1.76798C6.67625 1.82453 6.69642 1.89102 6.69642 1.95903C6.69618 2.05017 6.65988 2.1375 6.59544 2.20194C6.531 2.26638 6.44366 2.30269 6.35253 2.30292ZM9.2085 3.59028C8.82242 3.59028 8.51048 3.90733 8.51048 4.29852V7.065C8.51048 7.45491 8.82369 7.77324 9.2085 7.77324C9.59457 7.77324 9.90651 7.45619 9.90651 7.065V4.29724C9.90779 3.90605 9.59585 3.59028 9.2085 3.59028ZM0.788893 3.59028C0.402813 3.59028 0.0908813 3.90733 0.0908813 4.29852V7.065C0.0908813 7.45491 0.404091 7.77324 0.788893 7.77324C1.17497 7.77324 1.4869 7.45619 1.4869 7.065V4.29724C1.4869 3.90605 1.17369 3.59028 0.788893 3.59028Z" fill="black" />
                                                </svg>
                                            </span>
                                        </button>
                                    </a>
                                    <a href="#" className="mo-rong">Mở rộng</a>
                                </div>
                            </div>
                        </React.Fragment>)
                    this.setState({
                        pageCount: Math.ceil(data.length / this.state.perPage),
                        postData
                    })
                });
        }
    };

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.receivedData()
        }); 

    };

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            callApi(`tdcmobileapp/${id}`, 'get', null).then(res => {
                var data = res.data.record;
                console.log(res.data);
                this.setState({
                    name: data.name,
                    iconURL: data.iconURL,
                    images: data.images,
                    description: data.description,
                    bundleName: data.bundleName,
                });
            });
            callApi(`tdcmobileapp/versionlist/${id}`, 'get', null).then(res => {
                this.setState({
                    versions: res.data
                });
            });
            this.receivedData()
        }
    }
    getVersionAndroidLate(versions) {
        var result = null;
        if (versions.length > 0) {
            versions.map((version, index) => {
                if (index === 0) {
                    result = version.android
                }
            });
        }
        return result;
    }
    getVersionIOSLate(versions) {
        var result = null;
        if (versions.length > 0) {
            versions.reverse().map((version, index) => {
                if (index === 0) {
                    result = version.ios;
                }
            });
        }
        return result;
    }
    getVersionVersionLate(versions) {
        var result = null;
        if (versions.length > 0) {
            versions.map((version, index) => {
                if (index === 0) {
                    result = version.version;
                }
            });
        }
        return result;
    }
    getVersionDescriptionLate(versions) {
        var result = null;
        if (versions.length > 0) {
            versions.map((version, index) => {
                if (index === 0) {
                    result = version.descripstion;
                }
            });
        }
        return result;
    }


    render() {
        var { name, iconURL, images, description, bundleName, versions } = this.state;
        var icon_URL = `${API_URL}/${iconURL}`;


        //Check phien ban
        var sttver = 'Các phiên bản khác';
        if (!versions.length > 0) {
            sttver = 'Chưa có phiên bản nào';
        }
        var elem = images.map((image, index) => {
            var icon_URL = `${API_URL}/${image.imageUrl}`;
            return (
                <button className="bg-image" style={{ backgroundImage: `url(${icon_URL})` }} />
            );
        });

        return (
            <div className="container-fluid rs-pd-mr">
                <div className="container">
                    <Row>
                        <div className="col-lg-12 col-md-12 col-12" id="getheight">
                            <div>
                                <div className='tdctable row'>   
                                    <div className='tdcimgapp col-hl-xl-2 col-sm-2 col-5 col-hl-lg-2'>
                                        <div className='tdcimg'>
                                            <img src={icon_URL} className='widthimg' />
                                        </div>
                                    </div>
                                    <div className='tdccontent col-lg-10 col-sm-10 col-7 col-xl-10'>
                                        <Row>
                                            <div className='tdccontentmg'>
                                                <div className='tdccontentmr'>
                                                    <h1 className='titleapp'>{name}</h1>
                                                </div>
                                            </div>
                                            <div role='button' className='contenttitle'>
                                                <span>
                                                    {bundleName}
                                                </span>
                                            </div>
                                        </Row>
                                        <div className='btndowload'>
                                            <Row>
                                                <div className='status-version tdccontentmg col-5'>{this.getVersionVersionLate(versions)}</div>
                                                <div className='status-version tdccontentmg col-7'>Phien ban khac</div>
                                            </Row>
                                            <a href={this.getVersionIOSLate(versions)}>
                                                <button className="btn btnversion btn-ios" style={{ marginRight: '10px' }}>
                                                    <span>
                                                        <svg width="11" height="25" viewBox="0 1 10 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M8.5194 6.79614C8.51363 5.81252 8.95928 5.07121 9.85925 4.52459C9.3559 3.80347 8.5944 3.40685 7.59059 3.33041C6.64014 3.25541 5.60028 3.88423 5.21953 3.88423C4.81714 3.88423 3.89698 3.35637 3.17297 3.35637C1.6788 3.37945 0.0908813 4.54767 0.0908813 6.9245C0.0908813 7.62688 0.219242 8.35233 0.475962 9.09942C0.819218 10.083 2.05667 12.493 3.34748 12.4541C4.02246 12.4382 4.49984 11.9753 5.37817 11.9753C6.23055 11.9753 6.67187 12.4541 7.42473 12.4541C8.72708 12.4353 9.84627 10.2446 10.1722 9.25806C8.42565 8.43454 8.5194 6.84662 8.5194 6.79614ZM7.00359 2.39727C7.73481 1.52904 7.66847 0.738683 7.64684 0.454559C7.00071 0.492058 6.25362 0.894446 5.82816 1.38914C5.35943 1.91989 5.08396 2.57611 5.14309 3.31599C5.84114 3.36935 6.47861 3.01023 7.00359 2.39727Z" fill="white" />
                                                        </svg>
                                                    </span>&nbsp;
                                                iOS
                                            </button>
                                            </a>
                                            <a href={this.getVersionAndroidLate(versions)}>
                                                <button className="btn btnversion btn-android">
                                                    <span>
                                                        <svg width="11" height="25" viewBox="0 2 10 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M1.90622 8.66429C1.90622 8.96343 2.1504 9.20761 2.45082 9.20761H3.07341V10.7468C3.07341 11.1367 3.38662 11.4551 3.77142 11.4551C4.1575 11.4551 4.46943 11.138 4.46943 10.7468V9.20633H5.55608V10.7455C5.55608 11.1355 5.86929 11.4538 6.25409 11.4538C6.64017 11.4538 6.9521 11.1367 6.9521 10.7455V9.20633H7.57469C7.87511 9.20633 8.11929 8.96216 8.11929 8.66301V3.61074H1.90622V8.66429ZM6.47142 0.992555L7.04543 0.161589C7.07867 0.11301 7.071 0.0478109 7.02625 0.0158507C6.98151 -0.0148311 6.91759 0.000509842 6.88435 0.0490894L6.28861 0.913294C5.89614 0.758606 5.45892 0.672953 4.99997 0.672953C4.54102 0.672953 4.10381 0.758606 3.71134 0.913294L3.1156 0.0503678C3.08236 0.00178823 3.01844 -0.0148311 2.97369 0.0171292C2.92895 0.047811 2.92128 0.111731 2.95452 0.162868L3.52852 0.993833C2.61574 1.41827 1.97653 2.22238 1.89472 3.16201H8.10779C8.02341 2.22111 7.38293 1.41699 6.47142 0.992555ZM3.68832 2.30292C3.62031 2.30292 3.55382 2.28276 3.49727 2.24497C3.44072 2.20718 3.39664 2.15347 3.37061 2.09063C3.34458 2.0278 3.33777 1.95865 3.35104 1.89194C3.36431 1.82523 3.39706 1.76396 3.44516 1.71586C3.49325 1.66777 3.55453 1.63502 3.62123 1.62175C3.68794 1.60848 3.75709 1.61529 3.81993 1.64132C3.88276 1.66735 3.93647 1.71142 3.97426 1.76798C4.01205 1.82453 4.03222 1.89102 4.03222 1.95903C4.03198 2.05017 3.99567 2.1375 3.93123 2.20194C3.86679 2.26638 3.77946 2.30269 3.68832 2.30292ZM6.35253 2.30292C6.28451 2.30292 6.21803 2.28276 6.16147 2.24497C6.10492 2.20718 6.06084 2.15347 6.03481 2.09063C6.00879 2.0278 6.00198 1.95865 6.01524 1.89194C6.02851 1.82523 6.06127 1.76396 6.10936 1.71586C6.15746 1.66777 6.21873 1.63502 6.28544 1.62175C6.35215 1.60848 6.42129 1.61529 6.48413 1.64132C6.54697 1.66735 6.60068 1.71142 6.63846 1.76798C6.67625 1.82453 6.69642 1.89102 6.69642 1.95903C6.69618 2.05017 6.65988 2.1375 6.59544 2.20194C6.531 2.26638 6.44366 2.30269 6.35253 2.30292ZM9.2085 3.59028C8.82242 3.59028 8.51048 3.90733 8.51048 4.29852V7.065C8.51048 7.45491 8.82369 7.77324 9.2085 7.77324C9.59457 7.77324 9.90651 7.45619 9.90651 7.065V4.29724C9.90779 3.90605 9.59585 3.59028 9.2085 3.59028ZM0.788893 3.59028C0.402813 3.59028 0.0908813 3.90733 0.0908813 4.29852V7.065C0.0908813 7.45491 0.404091 7.77324 0.788893 7.77324C1.17497 7.77324 1.4869 7.45619 1.4869 7.065V4.29724C1.4869 3.90605 1.17369 3.59028 0.788893 3.59028Z" fill="black" />
                                                        </svg>
                                                    </span>&nbsp;
                                                Android
                                            </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='info-app'>
                                <h5>Hình ảnh ứng dụng</h5>
                                <div className="slider-scroll">
                                    {elem}
                                </div>
                            </div>
                            <div className='info-app'>
                                <h5>Mô tả</h5>
                                <div className='description'>
                                    <div className='description-content'>
                                        {ReactHtmlParser(description)}
                                    </div>
                                </div>
                            </div>
                            <div className='info-app'>
                                <h5>Tính năng mới</h5>
                                <div className='description'>
                                    <div className='description-content'>
                                        {ReactHtmlParser(this.getVersionDescriptionLate(versions))}
                                    </div>
                                </div>
                            </div>
                            <div className='version-app' style={{ borderTop: "1px ridge" }}>
                                <h5 style={{ paddingTop: "10px" }}>Các phiên bản khác</h5>
                                {/* Version List */}
                                <div className="row">
                                    <div className="col-xl-12 col-sm-12 col-12">
                                        <div className="row">
                                            {this.state.postData}
                                            <ReactPaginate
                                                previousLabel={"<"}
                                                nextLabel={">"}
                                                breakLabel={"..."}
                                                breakClassName={"break-me"}
                                                pageCount={this.state.pageCount}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={5}
                                                onPageChange={this.handlePageClick}
                                                containerClassName={"pagination"}
                                                subContainerClassName={"pages pagination"}
                                                activeClassName={"active"} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Row>
                </div>
                {/* <div className="container-fluid" style={{height:"200px",width:"100%",background:"#333333",padding:"0px"}}>
                    <p style={{color:"#ffffff",bottom:"-50%",left:"85%",position:"relative"}}>Contact : Nguyễn Hoàng Long<br></br>
                    Email   : nguyenlong08112000@gmail.com<br></br>
                    Phone   : 0388049594</p>
                </div> */}
            </div>
        );
    }
}

export default VersionListPage;