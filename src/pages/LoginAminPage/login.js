import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import './../../css/login.css';
import apiCaller from './../../utils/apiCaller';
class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  formlogin() {
    document.getElementById("form-login").style.display = "block";
    document.getElementById("form-forgot").style.display = "none";
}
  formforgot() {
    document.getElementById("form-login").style.display = "none";
    document.getElementById("form-forgot").style.display = "block";
}
  onSubmit = (event) => {
    event.preventDefault();
    var { history } = this.props;
    var body = this.state;
    apiCaller('login', 'POST', body).then(res => {
      if (res) {
        if (res.status === 200) {
          window.localStorage.setItem('jwt_token', res.data.token_login);
          window.localStorage.setItem('email', this.state.email);
          window.localStorage.setItem('password', this.state.password);
          history.push(`/productmanager`);
          return res;
        }
      } else {
        alert('dang nhap khong thanh cong');
      }
    });
  }
  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })

  }
  render() {
    var { email, password } = this.state;
    return (
      <div className="container-fluid bg-custom">
        <div className="content-login">
          <div className="logo-dangnhap">
            <img src="https://www.fis.com.vn/Portals/_default/Skins/SSD/images/logo.png"/>
          </div>
          
          <form onSubmit={this.onSubmit} id="form-login">
            <header>Đăng Nhập</header>
            <div className="field">
              <span className="fa fa-user" aria-hidden="true" />
              <input type="text" required placeholder="Email của bạn" name="email" value={email} onChange={this.onChange} />
            </div>
            <div className="field space">
              <span className="fa fa-lock" aria-hidden="true" />
              <input type="password" className="pass-key" required placeholder="Mật khẩu" name="password" value={password} onChange={this.onChange} />
              <span className="show-pass">SHOW</span>
            </div>
            <div className="forgot-pass">
              <Link onClick={this.formforgot} className="pass-forgot">Quên mật khẩu?</Link>
            </div>
            <div className="fieldsubmit">
              <input type="submit" value="Login" className="submit-button" onClick={this.log} />
            </div>
          </form>
          <form onSubmit={this.onSubmit} id="form-forgot" style={{display:"none"}}>
            <header>Quên mật khẩu</header>
            <div className="field">
              <span className="fa fa-user" aria-hidden="true" />
              <input type="text" required placeholder="Email của bạn" name="email" value={email} onChange={this.onChange} />
            </div>
            <div className="fieldsubmit" style={{marginTop:"20px"}}>
              <input type="submit" value="Send Email" className="submit-button" onClick={this.log} />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default LoginPage;