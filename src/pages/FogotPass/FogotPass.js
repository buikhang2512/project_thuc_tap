import React, { Component } from 'react';
import userCallerApi from '../../utils/userCallerApi';
class FogotPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ''
        }
    }
    onChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })

    }
    onSubmit = (e) => {
        e.preventDefault();
        //var {history} = this.props;
        var { email } = this.state;
        e.preventDefault();
        userCallerApi('forgotpass/sendemail', 'POST', { email }).then(res => {
            if (res) {
                if (res.data.resetpass_token) {
                    window.localStorage.setItem('resetpass_token', res.data.resetpass_token);
                  
                    //history.push('/changPass');
                    alert('Xác nhận email!');
                    // console.log(res);
                }
                else
                {
                    alert('Eamil không hợp lệ!');
                }
            }
           
        })
    }
    render() {
        var { email } = this.state;
        return (
            <div className="doipass">
                <div className="col col-6 doi">
                    <div className="content-right aboutpass">
                        <h3 className="thongtin">Lấy lại mật khẩu</h3>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group row">

                                <div className="wrap-input">
                                    <span className="lable-input2" >Nhập email</span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" name="email" value={email} onChange={this.onChange}></input>
                                </div>
                                <button type="submit" className="btn btn-success mr-10" onClick={this.onEmail}>Gửi</button>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        );
    }
}
export default FogotPass;