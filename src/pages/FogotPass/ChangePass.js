import React,{Component} from 'react';
import userCallerApi from '../../utils/userCallerApi';
class ChangePass extends Component {
    constructor(props) {
        super(props);
        this.state ={
            password : '',
            comfimpass : ''
        }
    }
    onChange = (event) => {
        this.setState({
          [event.target.name]: event.target.value
        })
      }
    onSubmit=(e)=>
    {
        var {history} = this.props;
       // var resetpass = localStorage.getItem('resetpass_token');
        var {password,comfimpass}= this.state;
        e.preventDefault();
        if(password===comfimpass)
        {
            userCallerApi('forgotpass/token/','PUT',{password}).then(res=>{
                if(res)
                {
                    if(res.status===200)
                    {
                        alert("Thay đổi mật khẩu thành công!");
                        history.push('productmanager');
                    }
                }
                else
                {
                   alert('Không đổi được mật khẩu!');
                }
            })

        }
        else{
            alert('Mat khau khong trung khop');
        }
    }
    render() {
       var {password,comfimpass} = this.state;
        return (
            <div className="doipass">
            <div className="col col-6 doi">
                <div className="content-right aboutpass">
                    <h3 className="thongtin">Đổi mật khẩu mới</h3>
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group row">
                            <div className="wrap-input">
                                <span className="lable-input2">Mật khẩu mới  </span>
                                <span className="haicham">:</span>
                                <input className="input100" type="text" name="password" value={password} onChange={this.onChange}></input>
                            </div>
                            <div className="wrap-input">
                                <span className="lable-input2">xác nhận mật khẩu mới </span>
                                <span className="haicham">:</span>
                                <input className="input100" type="text" name="comfimpass" value={comfimpass} onChange={this.onChange}></input>
                            </div>
                            <button type="reset" className="btn btn-info mr-10">Làm lại</button>
                            <button type="submit" className="btn btn-success mr-10">Đổi mật khẩu</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
        );
    }
}
export default ChangePass;
