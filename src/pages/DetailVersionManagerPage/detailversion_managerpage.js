import React from 'react';
import './../../css/styletdc.css';
import { Button, FormControl, Nav, Form, Navbar, Row, Col, Container } from 'react-bootstrap';
import '../ProductActionPage/node_modules/bootstrap/dist/css/bootstrap.min.css';
import ProductListMagager from '../../components/Manager/productlist_manage';
import {Link} from 'react-router-dom';

class DetailVersionManagerPage extends React.Component {
    render() {
        return (
            <Container>
                <Row style={{ marginTop: '20px' }}>
                    <Col xs={6} md={6} lg={6} sm={6}>
                        <Form>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="name@example.com" />
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Example textarea</Form.Label>
                            <Form.Control as="textarea" rows={3} />
                        </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }

}

export default DetailVersionManagerPage;