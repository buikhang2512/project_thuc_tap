import React from 'react';
class AdminTDC extends React.Component {
    render() {
        return (
            <div className="container-fluid" style={{paddingLeft:"0px",paddingRight:"0px"}}>
            <aside className="left-sidebar">
                {/* Sidebar scroll*/}
                <div className="top-image-name">
                    <strong>TDC Manage App</strong>
                </div>
                <div className="scroll-sidebar">
                    {/* Sidebar navigation*/}
                    <nav className="sidebar-nav">
                        <ul id="sidebarnav" className="p-t-30 in">
                            <li className="sidebar-item selected"> <a className="sidebar-link waves-effect waves-dark sidebar-link active" href="index.html" aria-expanded="false"><i className="mdi mdi-view-dashboard" /><span className="hide-menu">Thống kê</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link waves-effect waves-dark sidebar-link" href="charts.html" aria-expanded="false"><i className="mdi mdi-chart-bar" /><span className="hide-menu">Charts</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link waves-effect waves-dark sidebar-link" href="widgets.html" aria-expanded="false"><i className="mdi mdi-chart-bubble" /><span className="hide-menu">Widgets</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link waves-effect waves-dark sidebar-link" href="tables.html" aria-expanded="false"><i className="mdi mdi-border-inside" /><span className="hide-menu">Tables</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link waves-effect waves-dark sidebar-link" href="grid.html" aria-expanded="false"><i className="mdi mdi-blur-linear" /><span className="hide-menu">Full Width</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-receipt" /><span className="hide-menu">Forms </span></a>
                                <ul aria-expanded="false" className="collapse  first-level">
                                    <li className="sidebar-item"><a href="form-basic.html" className="sidebar-link"><i className="mdi mdi-note-outline" /><span className="hide-menu"> Form Basic </span></a></li>
                                    <li className="sidebar-item"><a href="form-wizard.html" className="sidebar-link"><i className="mdi mdi-note-plus" /><span className="hide-menu"> Form Wizard </span></a></li>
                                </ul>
                            </li>
                            <li className="sidebar-item"> <a className="sidebar-link waves-effect waves-dark sidebar-link" href="pages-buttons.html" aria-expanded="false"><i className="mdi mdi-relative-scale" /><span className="hide-menu">Buttons</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-face" /><span className="hide-menu">Icons </span></a>
                                <ul aria-expanded="false" className="collapse  first-level">
                                    <li className="sidebar-item"><a href="icon-material.html" className="sidebar-link"><i className="mdi mdi-emoticon" /><span className="hide-menu"> Material Icons </span></a></li>
                                    <li className="sidebar-item"><a href="icon-fontawesome.html" className="sidebar-link"><i className="mdi mdi-emoticon-cool" /><span className="hide-menu"> Font Awesome Icons </span></a></li>
                                </ul>
                            </li>
                            <li className="sidebar-item"> <a className="sidebar-link waves-effect waves-dark sidebar-link" href="pages-elements.html" aria-expanded="false"><i className="mdi mdi-pencil" /><span className="hide-menu">Elements</span></a></li>
                            <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-move-resize-variant" /><span className="hide-menu">Addons </span></a>
                                <ul aria-expanded="false" className="collapse  first-level">
                                    <li className="sidebar-item"><a href="index2.html" className="sidebar-link"><i className="mdi mdi-view-dashboard" /><span className="hide-menu"> Dashboard-2 </span></a></li>
                                    <li className="sidebar-item"><a href="pages-gallery.html" className="sidebar-link"><i className="mdi mdi-multiplication-box" /><span className="hide-menu"> Gallery </span></a></li>
                                    <li className="sidebar-item"><a href="pages-calendar.html" className="sidebar-link"><i className="mdi mdi-calendar-check" /><span className="hide-menu"> Calendar </span></a></li>
                                    <li className="sidebar-item"><a href="pages-invoice.html" className="sidebar-link"><i className="mdi mdi-bulletin-board" /><span className="hide-menu"> Invoice </span></a></li>
                                    <li className="sidebar-item"><a href="pages-chat.html" className="sidebar-link"><i className="mdi mdi-message-outline" /><span className="hide-menu"> Chat Option </span></a></li>
                                </ul>
                            </li>
                            <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-account-key" /><span className="hide-menu">Authentication </span></a>
                                <ul aria-expanded="false" className="collapse  first-level">
                                    <li className="sidebar-item"><a href="authentication-login.html" className="sidebar-link"><i className="mdi mdi-all-inclusive" /><span className="hide-menu"> Login </span></a></li>
                                    <li className="sidebar-item"><a href="authentication-register.html" className="sidebar-link"><i className="mdi mdi-all-inclusive" /><span className="hide-menu"> Register </span></a></li>
                                </ul>
                            </li>
                            <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-alert" /><span className="hide-menu">Errors </span></a>
                                <ul aria-expanded="false" className="collapse  first-level">
                                    <li className="sidebar-item"><a href="error-403.html" className="sidebar-link"><i className="mdi mdi-alert-octagon" /><span className="hide-menu"> Error 403 </span></a></li>
                                    <li className="sidebar-item"><a href="error-404.html" className="sidebar-link"><i className="mdi mdi-alert-octagon" /><span className="hide-menu"> Error 404 </span></a></li>
                                    <li className="sidebar-item"><a href="error-405.html" className="sidebar-link"><i className="mdi mdi-alert-octagon" /><span className="hide-menu"> Error 405 </span></a></li>
                                    <li className="sidebar-item"><a href="error-500.html" className="sidebar-link"><i className="mdi mdi-alert-octagon" /><span className="hide-menu"> Error 500 </span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    {/* End Sidebar navigation */}
                </div>
                {/* End Sidebar scroll*/}
            </aside>
            <div className="thong-ke">
                <div className="menu-top-admin row">
                    <div className="col-2">
                        
                    </div>
                    <div className="col-7">
                    <img src="https://www.fis.com.vn/Portals/_default/Skins/SSD/images/logo.png" style={{paddingTop:"10px"}}></img>
                    </div>
                    <div className="col-3">
                    <i className="fa fa-search" style={{float:"left",paddingTop:"30px",fontSize:"18px",marginLeft:"40px"}}></i>
                    <div className="avatar-name"><p>L</p></div>
                    <p className="tenuser">Nguyễn Hoàng Long</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-2">
                    </div>
                    <div className="col-7">
                        <h3 style={{paddingTop:"10px"}}>Thống kê</h3>
                        <div className="element-thongke">
                            <i className="fa fa-android">       34 app</i>
                        </div>
                        <div className="element-thongke">
                            <i className="fa fa-user">1200 User</i>
                        </div> 
                    </div>
                </div>
                <div className="row" style={{paddingTop:"20px"}}>
                    <div className="col-2">
                    </div>
                    <div className="col-10 border-top">
                        <h3 style={{paddingTop:"10px"}}>Top app đươc xem nhiều nhất</h3>
                        <div className="row">
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row" style={{paddingTop:"20px"}}>
                    <div className="col-2">
                    </div>
                    <div className="col-10 border-top">
                        <h3 style={{paddingTop:"10px"}}>Top app đươc Tải nhiều nhất</h3>
                        <div className="row">
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                            <div className="col-4 pd-top-20px">
                                <div className="images-element">
                                    <img src="https://apps.fis.vn/apifis/images/icon/FIS-INSIGHT-002.png"></img>
                                </div>
                                <div className="content-element">
                                    <p>Fis_Product</p>
                                    <i className="fa fa-eye">2000</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

        )
    }
}

export default AdminTDC;
