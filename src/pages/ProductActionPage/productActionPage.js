import React from 'react';
import {Container, Row, Button, FormControl, Nav, Form, Navbar, OverlayTrigger, Tooltip } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import callApi from '../../utils/apiCaller';
import axios from 'axios'
import {API_URL} from './../../constants/Config'
import { Link } from 'react-router-dom'
import userCallerApi from './../../utils/userCallerApi';

class ProductActionPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedICON : null,
            selectedIMGS : [],
            id: '',
            txtNameApp: '',
            txtBundleName: '',
            txtDescription: '',
            products: [],
            imgSrc: '',
            imgs: [],
            imgsold:'',
            loadingfile:'',
        };
    }
    componentDidMount() {
        var { match,history } = this.props;
        var token = localStorage.getItem('jwt_token');
        if(token === null || token === undefined) { history.push('/loginadmin');}
        if (match) {
            var id = match.params.id;
            // console.log(id);
            callApi(`tdcmobileapp/${id}`, 'GET', null).then(res => {
                var data = res.data.record;
                this.setState({
                    id: data._id,
                    txtNameApp: data.name,
                    txtBundleName: data.bundleName,
                    txtDescription: data.description,
                    imgSrc: `${API_URL}/${data.iconURL}`,
                    imgsold: data.images,
                })
                console.log(this.state);
            });
        }
    }
onChange = (e) => {
    var target = e.target;
    var name = target.name;
    var value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
        [name]: value
    })
    // console.log(name , value);
}

onSave = (e) => {
    e.preventDefault();
    var { id, txtNameApp, txtBundleName, selectedICON, txtDescription ,selectedIMGS} = this.state;
    var { history } = this.props;
    const formData = new FormData();
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    
    if(selectedICON) { //gán icon
        formData.append(
            "icon", 
            selectedICON, 
            //ưselectedICON.name,
        )
        
    }
    if(selectedIMGS) { //gán hình cho carosel
        for (let i = 0; i < selectedIMGS.length; i++) {
            // console.log(selectedIMGS[i]);
            //img.push(selectedIMGS[i]);
            formData.append(`images`, selectedIMGS[i])
        }         
        //console.log(img);
    }
    if (id) {
        formData.append('name', txtNameApp);
        formData.append('bundleName', txtBundleName);
        formData.append('description', txtDescription);
        // console.log('dang update ne....')
        userCallerApi (`tdcmobileapp/update/${id}`,'PUT',formData,config).then(function (respone) { window.location.reload(false); alert('Sửa đổi thành công')
            //history.goBack(); //--di ve trang truoc do
            //history.push("/"); //--di toi duong dan ma ban truyen vao
        });
        this.setState({loadingfile: 'dashed-loading'})
    } else {
        formData.append('name', txtNameApp);
        formData.append('bundleName', txtBundleName);
        formData.append('description', txtDescription);

        // for(var pair of formData.entries()) {
        //     console.log(typeof pair[1]);
        //     console.log(pair[0]+ ', '+ pair[1]); 
        // }

        userCallerApi ('tdcmobileapp/create','POST',formData,config).then(function (respone) { window.location.reload(false); alert('Thêm thành công')})
        this.setState({loadingfile: 'dashed-loading'})
    }
}

onUpdate = (id) => {
    // var { products } = this.state;
}

onFileChange = (event) => {
    this.setState ({selectedICON: event.target.files[0]});
    var file = event.target.files[0];
    var reader = new FileReader();
    if(file && file.type.match('image.*')) {
        reader.readAsDataURL(file);
    }
    reader.onloadend = function (e) {
        this.setState({
            imgSrc: [reader.result]
        })
      }.bind(this);
    // TODO: concat files
}
onIMGSChange = (e) => {
    // const files = event.target.files;
    // this.setState({selectedIMGS: files});
    // var file = event.target.files[0];
    // var reader = new FileReader();
    // if(file && file.type.match('image.*')) {
    //     reader.readAsDataURL(file);
    // }
    // reader.onloadend = function (e) {
    //     this.setState({
    //         imgs: [reader.result]
    //     })
    //   }.bind(this);
    if (e.target.files) {

        /* Get files in array form */
        const files = Array.from(e.target.files);
        this.setState({selectedIMGS: files});
        /* Map each file to a promise that resolves to an array of image URI's */ 
        Promise.all(files.map(file => {
            return (new Promise((resolve,reject) => {
                const reader = new FileReader();
                reader.addEventListener('load', (ev) => {
                    resolve(ev.target.result);
                });
                reader.addEventListener('error', reject);
                reader.readAsDataURL(file);
            }));
        }))
        .then(images => {

            /* Once all promises are resolved, update state with image URI array */
            this.setState({ imgs : images })

        }, error => {        
            console.error(error);
        });
    }
}

buildImgTag(){
    return <Row>
    { 
    this.state.imgs.map(imageURI => 
    (<div className="col-lg-6 col-md-6 col-6">
        <img style={{width:'230px', marginTop:'20px'}} className="photo-uploaded" src={imageURI} alt="Photo uploaded"/>
    </div>)) 
    }
    </Row>
}
// onFileUpLoad = (e) => {
//     e.preventDefault();
//     const formData = new FormData();

//     const config = {
//         headers: {
//             'content-type': 'multipart/form-data'
//         }
//     }
//     if(this.state.selectedICON) {
//         formData.append(
//             "file", 
//             this.state.selectedICON, 
//             this.state.selectedICON.name,
//         )
//         axios.post(`${API_URL}/upload/icon`, formData,config);
//     }else {console.log('chọn hình dùm cái !!')}
// }

fileData = () => { 

    if (this.state.selectedICON) { 
      return ( 
        <div> 
          <h3>Icon Details:</h3> 
          <p>Icon Name: {this.state.selectedICON.name}</p> 
          <p>Icon Type: {this.state.selectedICON.type}</p>  
          <p> 
            Last Modified:{" "} 
            {this.state.selectedICON.lastModifiedDate.toDateString()} 
          </p> 
        </div> 
      ); 
    } else { 
      return ( 
        <div> 
          <p>Choose icon before Pressing the Upload button</p> 
          <br /> 
        </div> 
      ); 
    } 
  }; 

render() {
    var { txtNameApp, txtBundleName, txtDescription, imgSrc, imgs, imgsold,loadingfile} = this.state;
    const renderTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
          Có gì mới trong phiên bản này ?
        </Tooltip>
      );
    return (
        <Container>
            <Row style={{ marginTop: '20px' }}>
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h1>Add Product</h1>
                    <hr />
                    <Form onSubmit={this.onSave}>
                        {/* <Form.Group controlId="formBasicEmail">
                                <Form.Label>Mã sản phẩm</Form.Label>
                                <Form.Control type="text" placeholder="-" name="txtMasp"/>
                            </Form.Group> */}
                        <Form.Group controlId="formBasicEmail">
                        <Form.Label style={{fontWeight:'bold',fontSize:'1.2rem'}}>Name<i style={{color:'#ff0000'}}>*</i></Form.Label>
                            <Form.Control type="text" placeholder="-" name="txtNameApp" value={txtNameApp} onChange={this.onChange} />
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label style={{fontWeight:'bold',fontSize:'1.2rem'}}>Bundle Name</Form.Label>
                            <Form.Control type="text" placeholder="-" name="txtBundleName" value={txtBundleName} onChange={this.onChange} />
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label style={{fontWeight:'bold',fontSize:'1.2rem'}}>Description </Form.Label>
                            <OverlayTrigger placement="right" delay={{ show: 250, hide: 400 }} overlay={renderTooltip}>
                            <span><svg width="2em" height="2em" viewBox="-2 3 25 16" class="bi bi-question-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                            </svg></span>
                            </OverlayTrigger>
                            <Form.Control as="textarea" rows={3} name='txtDescription' value={txtDescription} onChange={this.onChange}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.File style={{fontWeight:'bold',fontSize:'1.2rem'}} type='file' name='icon' id="exampleFormControlFile1" label="Icon App" onChange={this.onFileChange}/>
                        </Form.Group>

                        {this.fileData()} 

                        <Form.Group>
                            <Form.File style={{fontWeight:'bold',fontSize:'1.2rem'}} type='file' name='images' id="exampleFormControlFile1" label="Image Descripton" onChange={this.onIMGSChange} multiple/>
                        </Form.Group>

                        <Link to='/productmanager' className="btn btn-danger mr-3">Return</Link>
                        <Button variant="success" type="submit">Save</Button><div className={loadingfile} style={{with:'100%',}}></div>
                    </Form>
                    <i style={{color:'#ff0000', marginBottom:'0px'}}>Điền đầy đủ thông tin trước khi ấn nút</i>
                 </div>
                 <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h1>Preview icon</h1>
                    <div style={{width:'230px',minHeight:'130px',maxHeight:'auto',backgroundColor:'#5d585c'}}>
                        <img style={{width:'230px'}} src={imgSrc}/>
                    </div>
                    <h1>Images description</h1>
                        {this.buildImgTag()}
                 </div>
            </Row>
        </Container>
    );
}

}

export default ProductActionPage;