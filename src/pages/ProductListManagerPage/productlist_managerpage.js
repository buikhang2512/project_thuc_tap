import React from 'react';
import './../../css/styletdc.css';
import { Button, FormControl, Nav, Form, Navbar, Row, Col, Container, Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductItemMagager from '../../components/ProductList/productitem_manager';
import callApi from './../../utils/apiCaller'
import userCallerApi from './../../utils/userCallerApi';
import { Link, Redirect } from 'react-router-dom';

class ProductListManagerPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
    }

    componentDidMount() {
        var {history} = this.props;
        var token = localStorage.getItem('jwt_token');
        if(token === null || token === undefined) { history.push('/loginadmin');}
        callApi('tdcmobileapp', 'get', null).then(res => {
            if (res) {
                this.setState({
                    products: res.data.record
                });
            }
        });    
    }
    onDelete = (id) => {
        var { products } = this.state;
        userCallerApi(`tdcmobileapp/delete/${id}`, 'DELETE', null, null).then(res => {
            // console.log("xoá",res);
            if(res)
            {
                if (res.status === 200) {
                    products = products.filter(product => product._id !== id);
                    this.setState({
                        products: products
                    })
                }
                alert('Xoá thành công');
            }else { alert('Bạn phải đăng nhập để sử dụng tính năng này !')}
        })
    }

    render() {
        var { products } = this.state;
        return (
            <Container>
                <Row style={{ marginTop: '20px' }}>
                    <Col xs={12} md={12} lg={12} sm={12}>
                        <Link to="/product/add" className="btn btn-success" style={{ marginBottom: '10px', marginLeft: '15px' }}>Add product</Link>{' '}
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="panel panel-success">
                                <div className="panel-heading">
                                    <h3 className="panel-title">List product</h3>
                                </div>
                                <div className="panel-body" className="border border-secondary rounded">
                                    <Table responsive>
                                        <thead className="thead-dark">
                                            <tr>
                                                <th>STT</th>
                                                <th>Name App</th>
                                                <th>Bundle Name</th>
                                                <th>Icon App</th>
                                                {/* <th>Status</th> */}
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.showProducts(products)}
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }

    showProducts(products) {
        var result = null
        if (products.length > 0) {
            result = products.map((product, index) => {
                return (<ProductItemMagager key={index} index={index} productmng={product} onDelete={this.onDelete} />);
            });
        }else {
            result = <td colSpan="5" >< h1 style = {{ color: 'red', fontSize: '5em', fontWeight: 'bold', textAlign: 'center' } } > Lỗi kết nối! </h1></td>;
        }
        return result;
    }
}


export default ProductListManagerPage;