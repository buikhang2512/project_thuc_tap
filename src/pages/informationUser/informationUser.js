import './../../css/InformationUser.css';
import React, { Component } from 'react';
import userCallerApi from '../../utils/userCallerApi';
class InformationUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: ''
        };
    }
    componentDidMount() {
        var {history} = this.props;
        var email = localStorage.getItem('email');
        var password = localStorage.getItem('password');
        var body = { email, password };
        var token = localStorage.getItem('jwt_token')
        if(token === null || token === undefined) { history.push('/loginadmin');}
        userCallerApi('user/getuser', 'GET', body)
            .then(res => {
                if(res)
                {
                    if(res.status===200)
                    {
                        this.setState({
                            users: res.data
                        })
                    }
                }
                else{
                    alert('Bạn phải đăng nhập!');
                    history.push('/loginadmin');
                }
                    
            })
    }
    
    logout=()=>
    {
        
        var {history} = this.props;
        localStorage.removeItem('jwt_token');
        localStorage.removeItem('email');
        localStorage.removeItem('password');
        history.push(`/loginadmin`);

        // userCallerApi('logout','post',null).then(res => {
        //     if(res.status === 200){
        //         var {history} = this.props;
        //         localStorage.removeItem('jwt_token');
        //         localStorage.removeItem('email');
        //         localStorage.removeItem('password');
        //         history.push(`/loginadmin`);
        //     }
        // })
        //console.log('hihi');
    }
    changePass =()=>
    {
        var {history} = this.props;
        history.push('/doipass')
    }
    render() {
        var { users } = this.state;
        return (
          
            <div className="content5">
               
                <div className="row chua">
               
                    <div className="col col-4">
                        <img src="https://static2.yan.vn/YanNews/2167221/202003/dan-mang-du-trend-thiet-ke-avatar-du-kieu-day-mau-sac-tu-anh-mac-dinh-b0de2bad.jpg"
                            className="img-fluid" alt="./img/nen2.jpg" />
                    </div>
                    <div className="col col-6">
                        <div className="content-right about">
                            <h3 className="thongtin">Thông tin người dùng</h3>

                            <div className="form-group row">

                                {/* <div className="wrap-input">
                                    <span className="lable-input">id</span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" readOnly value={users._id || ''}></input>
                                </div> */}

                                <div className="wrap-input">
                                    <span className="lable-input">User  </span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" readOnly value={users.user || ''}></input>
                                </div>

                                <div className="wrap-input">
                                    <span className="lable-input">Email </span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" readOnly value={users.email || ''}></input>
                                </div>

                                {/* <div className="wrap-input">
                                    <span className="lable-input">PassWord </span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" readOnly value={users.password} ></input>
                                </div> */}

                                <div className="wrap-input">
                                    <span className="lable-input">Phone </span>
                                    <span className="haicham">:</span>
                                    <input className="input100" type="text" readOnly value={users.phone || ''} ></input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col col-1">
                    <div className="group-button">
                <button type="button" onClick={this.logout} className="btn btn-danger mr-10">Đăng xuất</button>
                <button type="submit" onClick={this.changePass}className="btn btn-info mr-10">Đổi mật khẩu</button>
                </div>
                    </div>
                </div>
            </div>
            
        );
    }
}
export default InformationUser;