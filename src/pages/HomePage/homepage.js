import React from 'react';
import ProductList from './../../components/ProductList/productlist'
import callApi from './../../utils/apiCaller';
import notFound from './../../img/404_failed_loading_gray_wash.svg';
import IndexMenu from './../../components/Menu/NewMenu/IndexMenu'
class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            txtSearch: '',
            products: [],
            productsCreate: [],
            idDisplayform: true

        };
    }

    componentDidMount() {
        callApi('tdcmobileapp/newupdate', 'get', null).then(res => {
            if (res) {
                this.setState({
                    products: res.data.record
                });
            }
        });

        callApi('tdcmobileapp/newcreate', 'get', null).then(res => {
            if (res) {
                this.setState({
                    productsCreate: res.data.record
                });
            }
        });
    }
    offSearch=()=>{
       this.setState({
        idDisplayform : ! this.state.idDisplayform
       })
   
    }
    showSearch = () => {
       this.setState({
        idDisplayform : false
       })
    }
    onSearchApp =()=>{
        this.setState({
            idDisplayform : ! this.state.idDisplayform
        })
    }
    render() {
        var { products, productsCreate,idDisplayform } = this.state;
        var element = idDisplayform ?   '' : <div className="filter"/>;
        return (
            <div>
                <IndexMenu idDisplayform={idDisplayform} showSearch={this.showSearch} offSearch={this.offSearch} onSearchApp={this.onSearchApp}/>
                {/* <div className="contentvip"> */}
                    {element}
                    <div className='container-fruit'>
                        <div className="container" style={{ paddingBottom: "0px", height: "auto", marginBottom: "-570px" }}>
                            <h3 className="udmoicapnhat">Ứng dụng mới cập nhật</h3>
                            <div style={{ marginTop: '10px', paddingLeft: "0px", paddingRight: "0px" }} className='item-container br-top-1px'>
                                {this.showProducts(products)}
                            </div>
                        </div>
                        <div className="container">
                            <h3 className="udmoicapnhat">Ứng dụng mới đăng tải</h3>
                            <div style={{ marginTop: '10px', paddingLeft: "0px", paddingRight: "0px" }} className='item-container br-top-1px'>
                                {this.showProductsCreate(productsCreate)}
                            </div>
                        </div>
                    {/* </div> */}
                </div>
            </div>
        );
    }
    showProducts(products) {
        var result = null
        if (products.length > 0) {
            result = products.map((product, index) => {
                return (< ProductList key={index} product={product} />
                );
            });
        } else {
            result = <div style={{ margin: '0px auto' }}><img style={{ height: '112px' }} src={notFound} /><div><span>Trang này không thể hiển thị</span></div></div>;
        }
        return result;
    }
    showProductsCreate(productsCreate) {
        var result = null
        if (productsCreate.length > 0) {
            result = productsCreate.map((productCreate, index) => {
                return (< ProductList key={index} product={productCreate} />
                );
            });
        } else {
            result = <div style={{ margin: '0px auto' }}><img style={{ height: '112px' }} src={notFound} /><div><span>Trang này không thể hiển thị</span></div></div>;
        }
        return result;
    }
}
 
export default HomePage;