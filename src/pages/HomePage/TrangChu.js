import React from 'react';
import IndexMenu from './../../components/Menu/NewMenu/IndexMenu';
class TrangChu extends React.Component {
    render() {
        return (
            <div style={{backgroundColor:"#000000"}}>
                <IndexMenu style={{backgroundColor:"#0000000"}}/>
                <div className="container-fluid br-1px-sold" style={{width:"100vw",height:"100vh"}}>
                    <div className="row">
                        <div className="col-6">
                            <div style={{width:"622px",height:"auto"}} className="br-1px-sold text-content-homepage">
                                <h3 className="chud">Cửa hàng ứng dụng</h3>
                                <h3 className="FPT-Information">FPT Information System.</h3>
                                <p className="content-trangchu">Một chợ ứng dụng bao gồm các sản phẩm cho phép người dùng
                                tìm kiếm và tải về thiết bị di động của mình dưới sự quản lý của <p className="fis-trangchu">FIS</p>.
                                </p>
                                <a  href="#" className="xemsanpham">Xem sản phẩm</a><span className="fa fa-long-arrow-right spanMuiten"></span>
                            </div>
                        </div>
                        <div className="col-6" style={{height:"100vh",width:"100vw"}}>
                            <img className="image-trangchu" src = "http://10.86.153.77:6969/images/icon/Img.png"></img>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default TrangChu;