import React from 'react';
import './../../css/styletdc.css';
import { Button, FormControl, Nav, Form, Navbar, Row, Col, Container, Table, Tooltip, OverlayTrigger } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductItemMagager from '../../components/ProductList/productitem_manager';
import callApi from '../../utils/apiCaller'
import { Link } from 'react-router-dom';
import VersionItemMagager from '../../components/Version/versionitem_manager';
import userCallerApi from './../../utils/userCallerApi';

class VersionListManagerPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            versionmngs: '',
            idproduct:'',
            idapp:'',
            product:[],
            txtVersionName: '',
            txtDescriptionVer: '',
            selectionAndroid: null,
            selectionIOS: null,
            loadingfile: '',
        };
    }

    componentDidMount() {
        var {match,history} = this.props;
        var token = localStorage.getItem('jwt_token');
        if(token === null || token === undefined) { history.push('/loginadmin');}
        if(match) {
            var id = match.params.id;
            callApi(`tdcmobileapp/${id}`, 'get', null).then(res => {
                if (res) {
                    this.setState({
                        product: res.data.record
                    });
                }
            }); 
            callApi(`tdcmobileapp/versionlist/${id}`, 'get', null).then(res => {
                if(res) {
                    this.setState({
                        versionmngs: res.data.record,
                        idproduct: id
                    });
                }
            });
        }
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        })
        // console.log(name , value);
    }

    onIOSChange = (event) => {
        this.setState ({selectionIOS: event.target.files[0]});
        // console.log('chọn file ios',event.target.files);
    }

    onAndroidChange = (event) => {
        this.setState ({selectionAndroid: event.target.files[0]});
        // console.log('chọn file and',event.target.files);
    }

    // onTest = (e) => { e.preventDefault(); console.log('app',this.state.idapp);
    // console.log('product (chung)',this.state.idproduct);}
    onSave = (e) => {
        e.preventDefault();
        var { idproduct,idapp, txtVersionName, txtDescriptionVer, selectionIOS, selectionAndroid, loadingfile} = this.state;
        const formData = new FormData();
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        formData.append('version', txtVersionName);
        formData.append('descripstion', txtDescriptionVer);
        
        if(selectionIOS) {
            formData.append(
                "ios", 
                selectionIOS,
            ) 
        }

        if(selectionAndroid) {
            formData.append(
                "android", 
                selectionAndroid, 
            )
        }
        if(idapp) {
            userCallerApi (`tdcmobileapp/versionlist/update/${idproduct}/${idapp}`,'POST',formData,null).then(respone => 
                { window.location.reload(false); console.log('thanh cong', respone);}).catch( err => 
                    {console.log('that bai : (', err)});
            this.setState({loadingfile: 'dashed-loading'})
        }else {
            // console.log('nani');
            userCallerApi (`tdcmobileapp/versionlist/create/${idproduct}`,'POST',formData,null).then(respone => 
                { window.location.reload(false); console.log('thanh cong', respone);}).catch( err => 
                    {console.log('that bai : (', err)})
            this.setState({loadingfile: 'dashed-loading'})
        }
    }
    onUpdate = (id,name,des) => {
        this.setState({
            idapp:id,
            txtVersionName: name,
            txtDescriptionVer: des,     
        }) 
        // var {product} = this.state;
        // const formData = new FormData();
        // userCallerApi (`tdcmobileapp/versionlist/update/${product._id}/${id}`,'PUT',formData,null).then(respone => 
        //     { window.location.reload(false); console.log('thanh cong', respone);}).catch( err => 
        //         {console.log('that bai : (', err)});
    }

    onDelete = (id) => {
        var { versionmngs } = this.state;
        userCallerApi (`tdcmobileapp/versionlist/delete/${id}`,'DELETE',null,null).then(res => {
            if(res) {
                if (res.status === 200) {
                    versionmngs = versionmngs.filter(version => version._id !== id);
                    this.setState({
                        versionmngs: versionmngs
                    });
                    alert('Xoá thành công');
                }
            }
        })
    }

    showVersions(versionmngs) {
        var result = null;
        if (versionmngs.length > 0) {
            result = versionmngs.map((versionmng, index) => {    
                return (
                    <VersionItemMagager key={index} index={index} versionmng={versionmng} onDelete={this.onDelete} onUpdate={this.onUpdate}/>
                );
            });
        }else {
            result = <td colSpan="4" ><p style={{ color: 'red', fontSize: '2em', fontWeight: 'bold', textAlign: 'center' }}>Chưa có phiên bản nào</p><p style={{fontSize: '3em', fontWeight: 'bold', textAlign: 'center' }}>(☉_☉)</p></td>;
        }
        return result;
    }
    // onload = (e) => {
    //     console.log(e);
    //     this.setState({
    //         adu: 'dashed-loading',
    //     })
    // }
    render() {
        var {versionmngs,idapp,product,txtVersionName,txtDescriptionVer,loadingfile} = this.state;
        const {
            selectedFiles,
            currentFile,
            progress,
            message,
            fileInfos,
          } = this.state;
          const renderTooltip = (props) => (
            <Tooltip id="button-tooltip" {...props}>
              Có gì mới trong phiên bản này ?
            </Tooltip>
          );
        return (
            <Container>
                <Row style={{ marginTop: '20px' }}>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h1 style={{ marginBottom: '10px', marginLeft: '15px' }}>
                    <span>  <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-cloud-upload" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383z"/>
                        <path fill-rule="evenodd" d="M7.646 4.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V14.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3z"/>
                    </svg></span> Upload version
                    </h1>{' '}
                    <Form onSubmit= {this.onSave}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label style={{fontWeight:'bold',fontSize:'1.2rem'}}>Version</Form.Label>
                            <Form.Control type="text" placeholder="-" name="txtVersionName" value={txtVersionName} onChange={this.onChange}/>
                            <Form.Text className="text-muted">
                                *Điền đầy đủ thông tin vào trước khi ấn nút
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label style={{fontWeight:'bold',fontSize:'1.2rem'}}>Description</Form.Label>
                            <OverlayTrigger placement="right" delay={{ show: 250, hide: 400 }} overlay={renderTooltip}>
                            <span><svg width="2em" height="2em" viewBox="-2 3 25 16" class="bi bi-question-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                            </svg></span>
                            </OverlayTrigger>
                            <Form.Control as="textarea" rows={3}  name="txtDescriptionVer" value={txtDescriptionVer} onChange={this.onChange}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.File style={{fontWeight:'bold',fontSize:'1.2rem'}} type='file' name='file' id="exampleFormControlFile1" ref="fileInput" label="IOS" onChange={this.onIOSChange}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.File type='file' name='file' id="exampleFormControlFile1" label="Android" style={{fontWeight:'bold',fontSize:'1.2rem'}} onChange={this.onAndroidChange}/>
                        </Form.Group>
                        <div>
                        <Link to='/productmanager' className="btn btn-danger mr-3">Return</Link>
                        <Button variant="success" type="submit"> Save </Button><div className={loadingfile} style={{with:'100%',}}></div>
                        </div>
                    </Form> 
                    
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div className="panel panel-success">
                            <div className="panel-heading">
                                <h3 className="panel-title">{`List version ${product.name}`}</h3>
                            </div>
                            <div className="panel-body" class="border border-secondary rounded">
                                <Table responsive>
                                    <thead className='thead-dark'>
                                        <tr>
                                            <th>STT</th>
                                            <th>Version</th>
                                            <th>Date</th>
                                            {/* <th>Icon App</th> */}
                                            {/* <th>Status</th> */}
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.showVersions(versionmngs)}
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </div>
                </Row>
            </Container>
        );
    }
}


export default VersionListManagerPage;